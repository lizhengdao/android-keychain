# Android KeyChain
This is a simple facade on a native Android library to access the KeyStore and SharedPreferences.
Together they simulate a keychain like functionality.
It Offers a Utility class KeyChain for easy access.
But you can also use the PasswordManager, KeyStoreAccess and SharedPreferencesAccess to access them directly.

# Usage
You just need to supply the KeyChain class with an (Android KeyStore) Alias string and a string that serves as a key in the (Android) SharedPreferences. (<b>This is not Unity PlayerPrefs!</b>)

# Testing
This library only works on a host android system so you will not be able to test anything in your Unity3D editor.
The library itself is unit-tested in the android project.

# Source Code
The source code can be found [here](https://gitlab.com/ruben.hamers/native-android-passwordmanager)