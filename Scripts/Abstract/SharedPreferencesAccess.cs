namespace HamerSoft.AndroidKeyChain
{
    public class SharedPreferencesAccess : AbstractAndroidInterop
    {
        protected override string AndroidClassName => "SharedPreferencesAccess";

        public SharedPreferencesAccess(params object[] objects) : base(objects)
        {
        }

        public void Put(string key, string value)
        {
            Instance.Call("put", new object[] {key, value});
        }

        public string Get(string key)
        {
            return Instance.Call<string>("get", new object[] {key});
        }

        public void Delete(string key)
        {
            Instance.Call("delete", new object[] {key});
        }

        public bool HasValue(string key)
        {
            return Instance.Call<bool>("hasValue", new object[] {key});
        }
    }
}